import wx

from wbBase.application import App
from wbBase.applicationInfo import ApplicationInfo, PluginInfo

appinfo = ApplicationInfo(
    Plugins=[PluginInfo(Name="namespace", Installation="default")]
)



def test_plugin():
    app = App(test=True, info=appinfo)
    assert "namespace" in app.pluginManager
    app.Destroy()

def test_shellwin_instance():
    app = App(test=True)
    from wbpNamespace.control import NameSpace
    nameSpacewin = NameSpace(app.TopWindow)
    assert isinstance(nameSpacewin, NameSpace)
    app.Destroy()
