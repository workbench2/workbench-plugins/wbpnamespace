
Welcome to the wbpNamespace documentation!
===============================================================================

wbpNamespace is a plugin for `WorkBench <https://workbench2.gitlab.io/wbbase/>`_ 
applications. It adds a panel which allows you to explore the namespace of the
running Workbench application.

.. figure:: _static/namespace.png
   :scale: 100 %
   :alt: Namespace Panel screenshot

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   wbpNamespace

Indices and tables
-------------------------------------------------------------------------------

* :ref:`genindex`
* :ref:`modindex`
